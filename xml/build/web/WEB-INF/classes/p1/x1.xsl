<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body>
        <h1 align="left">          
          <xsl:value-of select="content/entity/@name"/>
        </h1>
       <xsl:value-of select="content/entity/country"/>
       &gt; 
      <xsl:value-of select="content/entity/sector"/>

      <h2 style="color: blue" align="center">          
        <xsl:for-each select="content/ratingCategory">
          <xsl:if test="@ratingTypeCode='ICR'">       
            <xsl:for-each select="rating">     
              <xsl:if test="name='Final'">
                <xsl:if test="creditWatch='Y'">
                  CW:
                </xsl:if>             
                <xsl:value-of select="outlook"/>              
                <xsl:value-of select="ratingCode"/>                
              </xsl:if>
            </xsl:for-each>
          </xsl:if>         
        </xsl:for-each>                      
      </h2>
        <xsl:value-of select="content/comments"/>        
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        <table align="center" border="1">
         
          <tr style="color: green">
            <th></th>            
              <th>Issuer Credit Rating</th>
              <th>Foreign Currency Rating</th>
              <th>Senior Securities Rating</th>                        
          </tr>
         

          <tr>
            <th style="color: red">Existing</th>                      
             <xsl:for-each select="content/ratingCategory">
                <th>               
                 <xsl:for-each select="rating">     
                   <xsl:if test="name='Existing'">
                     <xsl:if test="creditWatch='Y'">
                       CW:
                     </xsl:if>             
                     <xsl:value-of select="outlook"/>              
                     <xsl:value-of select="ratingCode"/>                
                   </xsl:if>
                 </xsl:for-each>
                </th>  
             </xsl:for-each>             
          </tr>          


          <tr>
              <th style="color: red">Recommended</th>                                                                      
              <xsl:for-each select="content/ratingCategory">
                 <th>               
                  <xsl:for-each select="rating">     
                    <xsl:if test="name='Recommended'">
                      <xsl:if test="creditWatch='Y'">
                        CW:
                      </xsl:if>             
                      <xsl:value-of select="outlook"/>              
                      <xsl:value-of select="ratingCode"/>                
                    </xsl:if>
                  </xsl:for-each>
                 </th>  
              </xsl:for-each>
          </tr>
          <tr>
              <th style="color: red">Final</th>            
              <xsl:for-each select="content/ratingCategory">
                 <th>               
                  <xsl:for-each select="rating">     
                    <xsl:if test="name='Final'">
                      <xsl:if test="creditWatch='Y'">
                        CW:
                      </xsl:if>             
                      <xsl:value-of select="outlook"/>              
                      <xsl:value-of select="ratingCode"/>                
                    </xsl:if>
                  </xsl:for-each>
                 </th>  
              </xsl:for-each>
          </tr>
        </table>

        <p></p>
        <p>Recommended by:</p>
        <p></p>
        <h3>
          Chairman : 
          <xsl:for-each select="content/entity/committeeMember/memberInfo">
            <xsl:if test="role='chairman'">
                <xsl:value-of select="@name"/>
            </xsl:if>
          </xsl:for-each>            
        </h3>
        <h3>
          Voters :
          <xsl:for-each select="content/entity/committeeMember/memberInfo">
            <xsl:if test="role='voter'">
                <xsl:value-of select="@name"/>
            </xsl:if>
          </xsl:for-each>            
        </h3>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>


