package p1;

/*
Author: pulkit.itp@gmail.com
XML to html
Compiler: JC 6
Server: Apache
*/

import javax.xml.transform.*;
//install javax.transform library if former version of JVM is used
import java.io.*;

public class xml {

public void convert(String file) {
  System.out.println("Program started....");
  try {
    TransformerFactory tFactory = TransformerFactory.newInstance();
    Transformer transformer =
      tFactory.newTransformer
         (new javax.xml.transform.stream.StreamSource
            ("c:/x1.xsl"));

    transformer.transform
      (new javax.xml.transform.stream.StreamSource
            (file),
       new javax.xml.transform.stream.StreamResult
            ( new FileOutputStream("c:/x1.html")));
    }
  catch (Exception e) {
    //System.out.println("erorrr...ppulkit catched");
    e.printStackTrace( );     //comment this line when use in production.
  }
  }
}